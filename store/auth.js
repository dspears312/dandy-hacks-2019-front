export const state = () => ({
  user: '',
  token: localStorage.token
})

export const mutations = {
  SET_USER(state, user) {
    state.user = user
  },
  SET_TOKEN(state, token) {
    state.token = token
    localStorage.token = token
  }
}