export const state = () => ({
    recipes: [
        { id: 1, name: "Ultimate Mexican Tacos", time: '30m', servings: 5 },
        { id: 2, name: "Spicy Chili", time: '1h', servings: 8 },
        { id: 3, name: 'Roast Beef', time: '25m', servings: 2 },
        { id: 4, name: 'Deluxe Chocolate Cake', time: '1h 30m', servings: 18 },
        { id: 5, name: 'Seafood Enchiladas', time: '45m', servings: 7},
        { id: 6, name: 'Chicken Tortilla Soup', time: '2h', servings: 20 },
        { id: 7, name: 'Barbeque Bacon Burgers', time: '20m', servings: 4 },
        { id: 8, name: 'Classic Italian Pizza', time: '1h 15m', servings: 4 }
    ]
})